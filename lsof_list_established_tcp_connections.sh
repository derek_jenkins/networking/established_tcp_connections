{
    echo "COUNT COMMAND PID USER TYPE PORT CONNECTION_FROM <-- ESTABLISHED_CONNECTIONS_ON_PORTS_80|443"
    {
        lsof -i TCP:80 -i TCP:443 -P | sed -e s'/:/ /g' -e s'/->/ /g' | awk '/ESTABLISHED/ {print $1, $2, $3, $5, $10, $11}' | sort | uniq -c | sort -nr
    }
    {
        echo "COUNT COMMAND PID USER TYPE PORT CONNECTION_FROM <-- ESTABLISHED_CONNECTIONS_ON_OTHER_PORTS"
        {
            lsof -i TCP -P | grep -vE ":80|:443|sshd" | sed -e s'/:/ /g' -e s'/->/ /g' | awk '/ESTABLISHED/ {print $1, $2, $3, $5, $10, $11}' | sort | uniq -c | sort -nr
        }
    }
} | column -t
