ss_list_established_tcp_connections() {
    echo 'Established Port 80/443 Connections (greater than five):'
    ss -tan | awk '/ESTAB/ {print $5}' | grep -E ":80|:443" | tr -s ':\|f' ' ' | sort | uniq -c | sort -nr | awk '$1 > 5'
    echo 'Established NON-Port 80/443 Connections (greater than five):'
    echo
    ss -tan | awk '/ESTAB/ {print $5}' | grep -vE ":80|:443" | tr -s ':\|f' ' ' | sort | uniq -c | sort -nr | awk '$1 > 5'
    echo
}
